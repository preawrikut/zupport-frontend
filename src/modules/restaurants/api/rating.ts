// posts.js
import axios from 'axios'

const API_URL = window.URL_API

export const apiRating = {
  async stackRating(formData:FormData) {
    const response = await axios.post(API_URL + '/api/ratings', formData)
    return response.data;
  },
  async getPopularRating(params:any) {
    const response = await axios.get(API_URL + '/api/ratings/popular', {
      params: params 
    })
    return response.data;
  }
}