// posts.js
import axios from 'axios'

const API_URL = window.URL_API

export const apiMap = {
  async getSuggests(params: any) {
    const response = await axios.get(API_URL + '/api/maps/suggests', {
      params: params 
    })
    return response.data.data;
  },
  async getRestaurants(params:any) {
    const response = await axios.get(API_URL + '/api/maps/restaurants', {
      params: params 
    })
    return response.data.data;
  },
  
}

