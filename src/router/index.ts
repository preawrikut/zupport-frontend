import { createRouter, createWebHistory } from 'vue-router'

import RouterRestaurants from '../modules/restaurants/router'

const routes = [
  ...RouterRestaurants
];
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
