const http = require("http");
const cors = require("cors");
const express = require("express");
const app = express();
app.use(cors());

const PORT = 40103;

const corsProxy = (req, res) => {
  const url = req.url.replace("", "");
  http.get(url, (response) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    response.pipe(res);
  });
};